export const formatTimeSheetAsArray = (
  timesheet: { date: string; activity: string; note: string }[]
) => {
  return timesheet.map((ts) => {
    return Object.values(ts);
  });
};
