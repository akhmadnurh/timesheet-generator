import dayjs from "dayjs";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";
import "dayjs/locale/id";

dayjs.locale("id");
dayjs.extend(isSameOrBefore);

export const generateDate = (period: string) => {
  const date = dayjs(period);

  const firstDate: number = date.startOf("month").get("date");
  const lastDate: number = date.endOf("month").get("date");

  let datesInMonth: string[] = [];

  let currentDate = firstDate;

  while (currentDate <= lastDate) {
    datesInMonth = [
      ...datesInMonth,
      date.set("date", currentDate).format("DD/MM/YYYY"),
    ];

    currentDate++;
  }

  return datesInMonth;
};
