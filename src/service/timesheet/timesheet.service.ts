import { create } from 'zustand';
import { TimeSheetType } from '../interface/timesheet.interface';
import { generateDate } from '../helper/date-generator.helper';
import dayjs from 'dayjs';
import * as ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';
import { formatTimeSheetAsArray } from '../helper/timesheet.helper';

interface TimeSheetStore {
  timesheet: TimeSheetType[];
  fullname: string;
  period: string;
  findTimeSheet: () => void;
  generateTimeSheet: (fullname: string, period: string) => boolean;
  updateTimeSheet: (date: string, type: string, value: string) => void;
  resetTimeSheet: () => void;
  downloadTimeSheet: () => void;
}

const temp = [
  { name: 'George Washington', birthday: '1732-02-22' },
  { name: 'John Adams', birthday: '1735-10-19' },
  // ... one row per President
];

export const useTimeSheetStore = create<TimeSheetStore>()((set, get) => ({
  timesheet: [] as TimeSheetType[],
  fullname: '' as string,
  period: '' as string,
  findTimeSheet() {
    const timesheet = localStorage.getItem('timesheet');
    const fullname = localStorage.getItem('fullname');
    const period = localStorage.getItem('period');

    if (timesheet && fullname && period) {
      set({ timesheet: JSON.parse(timesheet), fullname, period });
    }
  },
  generateTimeSheet(fullname, period) {
    const dates = generateDate(period);

    let timesheet: TimeSheetType[] = [];
    dates.forEach((date) => {
      timesheet = [
        ...timesheet,
        {
          date,
          activity: '',
          note: '',
        },
      ];
    });

    period = dayjs(period).locale('en').format('MMMM YYYY');

    localStorage.setItem('timesheet', JSON.stringify(timesheet));
    localStorage.setItem('fullname', JSON.stringify(fullname));
    localStorage.setItem('period', JSON.stringify(period));

    set({
      timesheet,
      fullname,
      period,
    });

    return true;
  },
  updateTimeSheet(date, type, value) {
    let timesheet = get().timesheet;

    timesheet = timesheet.map((item) => {
      if (item?.date === date) {
        return { ...item, [type]: value };
      }

      return item;
    });

    localStorage.setItem('timesheet', JSON.stringify(timesheet));

    set({ timesheet });
  },
  resetTimeSheet() {
    localStorage.clear();

    set({ timesheet: [], fullname: '', period: '' });
  },
  async downloadTimeSheet() {
    // Current period
    const currentPeriod = dayjs(get().period);
    const startDate = currentPeriod.startOf('month').format('D MMMM YYYY');
    const lastDate = currentPeriod.endOf('month').format('D MMMM YYYY');

    // Format timesheet into array
    const formattedTimeSheet = formatTimeSheetAsArray(get().timesheet);

    // Init workbook
    const workbook = new ExcelJS.Workbook();

    // Workbook metadata
    workbook.creator = get().fullname;
    workbook.lastModifiedBy = get().fullname;
    workbook.created = dayjs().toDate();
    workbook.modified = dayjs().toDate();
    workbook.lastPrinted = dayjs().toDate();

    // Create worksheet
    const worksheet = workbook.addWorksheet('Sheet1');

    // Set columns width
    worksheet.getColumn('A').width = 25;
    worksheet.getColumn('B').width = 55;
    worksheet.getColumn('C').width = 35;

    // Title
    const row1 = worksheet.getRow(1);
    row1.font = { bold: true };
    row1.alignment = { horizontal: 'center', vertical: 'middle' };

    worksheet.mergeCells('A1:C1');

    worksheet.getCell('A1').value = `Timesheet - ${currentPeriod.format('MMMM')}`;

    // Name
    worksheet.getRow(3).values = ['Nama', get().fullname.replaceAll('"', '')];

    // Period
    worksheet.getRow(4).values = ['Periode', `${currentPeriod.format('MMMM')} (${startDate} - ${lastDate})`];

    // Table header
    const row6 = worksheet.getRow(6);
    row6.font = { bold: true };
    row6.alignment = { horizontal: 'center', vertical: 'middle' };
    row6.values = ['Tanggal', 'Aktivitas', 'Keterangan'];

    // Insert timesheet into file
    for (let row = 7; row < 7 + formattedTimeSheet.length; row++) {
      // Row height
      const currentRow = worksheet.getRow(row);
      currentRow.height = 20;

      for (let col = 1; col <= 3; col++) {
        // Format cell
        const currentCell = worksheet.getCell(row, col);
        currentCell.border = {
          bottom: { style: 'thin' },
          top: { style: 'thin' },
          right: { style: 'thin' },
          left: { style: 'thin' },
        };
        currentCell.alignment = {
          horizontal: row === 6 ? 'center' : col === 1 ? 'right' : 'left',
          vertical: 'middle',
        };

        // Skip because row 6 is a table header
        if (row === 6) {
          continue;
        }

        // Insert data
        currentCell.value = formattedTimeSheet[row - 7][col - 1];
      }
    }

    // Create buffer
    const buffer = await workbook.xlsx.writeBuffer();

    // Create blob from buffer
    const blob = new Blob([buffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    // Download file
    saveAs(blob, `Timesheet ${currentPeriod.format('MMMM YYYY')} (${get().fullname.replaceAll('"', '')}).xlsx`);
  },
}));
