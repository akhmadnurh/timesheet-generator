export interface TimeSheetType {
  date: string;
  activity: string;
  note: string;
}
