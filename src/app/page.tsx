"use client";

import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  Popconfirm,
  Row,
  Table,
  Typography,
} from "antd";
import type { ColumnsType } from "antd/es/table";
import styles from "./page.module.css";
import { DownloadOutlined, StopOutlined } from "@ant-design/icons";
import { TimeSheetType } from "@/service/interface/timesheet.interface";
import { useTimeSheetStore } from "@/service/timesheet/timesheet.service";
import { useEffect, useState } from "react";

export default function Page() {
  const {
    timesheet,
    fullname,
    period,
    generateTimeSheet,
    findTimeSheet,
    resetTimeSheet,
    updateTimeSheet,
    downloadTimeSheet,
  } = useTimeSheetStore();
  const [windowSize, setWindowSize] = useState({ width: 0, height: 0 });

  useEffect(() => {
    findTimeSheet();

    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }, []);

  const onFinish = async () => {
    if (await form.validateFields()) {
      const generate = generateTimeSheet(
        form.getFieldValue("fullname"),
        form.getFieldValue("period")
      );

      if (generate) {
        form.resetFields();
      }
    }
  };

  const [form] = Form.useForm();

  const columns: ColumnsType<TimeSheetType> = [
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
    {
      title: "Activity",
      dataIndex: "activity",
      key: "activity",
      render(value, record, index) {
        return (
          <Input
            placeholder="Insert Activity"
            defaultValue={value}
            onChange={(e) => {
              updateTimeSheet(record?.date, "activity", e.target.value);
            }}
          />
        );
      },
    },
    {
      title: "Note",
      dataIndex: "note",
      key: "note",
      render(value, record, index) {
        return (
          <Input
            placeholder="Insert Note"
            defaultValue={value}
            onChange={(e) => {
              updateTimeSheet(record?.date, "note", e.target.value);
            }}
          />
        );
      },
    },
  ];

  return (
    <Row className={styles.container} gutter={[16, 16]}>
      {/* Reset Session */}
      <Popconfirm
        title="Delete Session"
        description="Are you sure to reset the session?"
        onConfirm={resetTimeSheet}
        okText="Sure"
        cancelText="No"
        placement={windowSize.width >= 992 ? "rightBottom" : "leftBottom"}
      >
        <Button
          style={{
            position: "absolute",
            [windowSize.width >= 992 ? "left" : "right"]: "1em",
            zIndex: 1,
            top: "1em",
            backgroundColor: "#ff4d4f",
            outlineColor: "none",
            border: "none",
          }}
          icon={<StopOutlined style={{ color: "white" }} />}
          size="small"
        ></Button>
      </Popconfirm>

      <Col
        span={24}
        lg={6}
        style={{
          backgroundColor: "#006d75",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingInline: "30px",
        }}
      >
        <Row>
          <Col span={24}>
            <Typography.Title
              level={3}
              className={styles.title}
              style={{ color: "white" }}
            >
              Timesheet Generator
            </Typography.Title>
          </Col>
          <Col span={24}>
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              name="timesheet-form"
            >
              <Row gutter={16}>
                <Col span={24}>
                  <Form.Item
                    rules={[{ required: true, message: "Insert full name!" }]}
                    label={<label style={{ color: "white" }}>Full Name</label>}
                    name={"fullname"}
                  >
                    <Input placeholder="Insert your name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    rules={[
                      { required: true, message: "Insert timesheet period!" },
                    ]}
                    label={<label style={{ color: "white" }}>Period</label>}
                    name={"period"}
                  >
                    <DatePicker
                      picker="month"
                      placeholder="Insert timesheet period"
                      className={styles.full}
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Row gutter={[16, 10]} style={{ marginBlock: "1em" }}>
                    <Col span={24} xl={12}>
                      {timesheet.length > 0 ? (
                        <Popconfirm
                          title="Replace Data"
                          description="Are you sure want to replace the current data?"
                          onConfirm={onFinish}
                          okText="Sure"
                          cancelText="No"
                          placement="bottom"
                        >
                          <Button type="primary" className={styles.full}>
                            Generate
                          </Button>
                        </Popconfirm>
                      ) : (
                        <Button
                          type="primary"
                          className={styles.full}
                          htmlType="submit"
                        >
                          Generate
                        </Button>
                      )}
                    </Col>
                    {timesheet.length > 0 && (
                      <Col span={24} xl={12} className={styles.download}>
                        <Button
                          icon={<DownloadOutlined />}
                          onClick={downloadTimeSheet}
                          className={styles.full}
                        >
                          Download
                        </Button>
                      </Col>
                    )}
                  </Row>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Col>
      <Col
        span={24}
        lg={18}
        style={{
          maxHeight: windowSize.width >= 992 ? "100vh" : "50vh",
          overflowY: "scroll",
          paddingInline: "1em",
          paddingBlock: "1em",
        }}
        // className={styles.timesheet_container}
      >
        <Typography.Text strong>Current Fullname: </Typography.Text>
        <Typography.Text>{fullname}</Typography.Text> <br />
        <Typography.Text strong>Period: </Typography.Text>
        <Typography.Text>{period}</Typography.Text>
        <Table
          dataSource={timesheet}
          columns={columns}
          className={styles.full}
          rowKey={"date"}
          pagination={false}
          bordered
        />
      </Col>
    </Row>
  );
}
